# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x >= 0 and x <= 10 and y >= 0 and y <= 10:
        return True
    return False


# Unit test
def is_inside_bounds_test():
    assert is_inside_bounds(-1, 15) is False
    assert is_inside_bounds(-1, 8) is False
    assert is_inside_bounds(1, 15) is False
    assert is_inside_bounds(1, 8) is True
    print("End of unit tests")


is_inside_bounds_test()
