# Write a function that meets these requirements.
#
# Name:       basic_calculator
# Parameters: left, the left number
#             op, the math operation to perform
#             right, the right number
# Returns:    the result of the math operation
#             between left and right
#
# The op parameter can be one of four values:
#   * "+" for addition
#   * "-" for subtraction
#   * "*" for multiplication
#   * "/" for division
#
# Examples:
#     * inputs:  10, "+", 12
#       result:  22
#     * inputs:  10, "-", 12
#       result:  -2
#     * inputs:  10, "*", 12
#       result:  120
#     * inputs:  10, "/", 12
#       result:  0.8333333333333334
def basic_calculator(left, op, righ):
    ops = {"+": lambda x, y: x + y, "-": lambda x, y: x - y,
           "/": lambda x, y: x / y, "*": lambda x, y: x * y}
    return ops[op](left, righ)


# Unit test
def basic_calculator_test():
    assert basic_calculator(10, "+", 12) == 22
    assert basic_calculator(10, "-", 12) == -2
    assert basic_calculator(10, "*", 12) == 120
    assert basic_calculator(10, "/", 12) == 0.8333333333333334
    print("End of unit tests")


basic_calculator_test()
