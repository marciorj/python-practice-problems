# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    return max(values) if len(values) else None


# Unit test
def calculate_grade_test():
    assert max_in_list([]) is None
    assert max_in_list([90, 95, 92, 89]) == 95
    assert max_in_list([88, 87, 91, 81]) == 91
    assert max_in_list([20, 47, 00, 11]) == 47
    print("End of unit tests")


calculate_grade_test()
