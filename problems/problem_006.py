# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >= 18 or has_consent_form is True:
        return True
    return False


# Unit test
def can_skydive_test():
    assert can_skydive(0, False) is False
    assert can_skydive(0, True) is True
    assert can_skydive(20, False) is True
    assert can_skydive(25, True) is True
    print("End of unit tests")


can_skydive_test()
