# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(val):
    roman_val = ['', 'I', 'II', 'III', 'IV', 'V',
                 'VI', 'VII', 'VIII', 'IX', 'X']
    if val < 1 or val > 10:
        return
    return roman_val[val]


# Unit test
def simple_roman_test():
    assert simple_roman(1) == "I"
    assert simple_roman(2) == "II"
    assert simple_roman(3) == "III"
    assert simple_roman(4) == "IV"
    assert simple_roman(5) == "V"
    assert simple_roman(6) == "VI"
    assert simple_roman(7) == "VII"
    assert simple_roman(8) == "VIII"
    assert simple_roman(9) == "IX"
    assert simple_roman(10) == "X"
    print("End of unit tests")


simple_roman_test()
