# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    return sum(values) if len(values) > 0 else None


# Unit test
def calculate_sum_test():
    assert calculate_sum([]) is None
    assert calculate_sum([1, 2, 3, 4]) == 10
    print("End of unit tests")


calculate_sum_test()
