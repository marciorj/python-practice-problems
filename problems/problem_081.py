# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


class Animal:
    def __init__(self, number_of_legs, primary_color):
        self.number_of_legs = number_of_legs
        self.primary_color = primary_color

    def describe(self):       # Returns a string that describes that animal
        return self.__class__.__name__ + \
            " has " + \
            str(self.number_of_legs) + \
            " legs and is primarily " + \
            self.primary_color


class Dog (Animal):
    def speak(self):          # Returns the string "Bark!"
        return "Bark!"


class Cat (Animal):
    def speak(self):          # Returns the string "Miao!"
        return "Miao!"


class Snake (Animal):
    def speak(self):          # Returns the string "Sssssss!"
        return "Sssssss!"


# Unit test
def class_all_animals_test():
    animal_list = [
        Dog(4, "brown"),
        Cat(4, "white"),
        Snake(0, "black")
    ]
    for animal in animal_list:
        print(animal.describe(), "-", animal.speak())
    print("End of unit tests")


class_all_animals_test()
