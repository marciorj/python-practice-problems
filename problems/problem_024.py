# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    return sum(values) / len(values) if len(values) > 0 else None


# Unit test
def calculate_average_test():
    assert calculate_average([]) is None
    assert calculate_average([1, 2, 3, 4]) == 2.5
    print("End of unit tests")


calculate_average_test()
