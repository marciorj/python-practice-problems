# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

from functools import reduce


def sum_of_squares(values):
    if len(values) == 0:
        return None
    return reduce(lambda ac, el: el * el + ac, values, 0)


# Unit test
def sum_of_squares_test():
    assert sum_of_squares([]) is None
    assert sum_of_squares([1, 2, 3]) == 14
    assert sum_of_squares([-1, 0, 1]) == 2
    assert sum_of_squares([3, 4, 2]) == 29
    print("End of unit tests")


sum_of_squares_test()
