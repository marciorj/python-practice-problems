# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    return 'fizz' if number and number % 3 == 0 else number


# Unit test
def is_divisible_by_3_test():
    assert is_divisible_by_3(0) == 0
    assert is_divisible_by_3(1) == 1
    assert is_divisible_by_3(3) == 'fizz'
    assert is_divisible_by_3(243) == 'fizz'
    assert is_divisible_by_3(7) == 7
    print("End of unit tests")


is_divisible_by_3_test()
