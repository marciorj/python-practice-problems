# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]
import math


def halve_the_list(in_list):
    lower_half = in_list[0: math.ceil(len(in_list) / 2)]
    upper_half = in_list[math.ceil(len(in_list) / 2):]
    return lower_half, upper_half


# Unit test
def halve_the_list_test():
    assert halve_the_list([1, 2, 3, 4]) == ([1, 2], [3, 4])
    assert halve_the_list([1, 2, 3]) == ([1, 2], [3])
    print("End of unit tests")


halve_the_list_test()
