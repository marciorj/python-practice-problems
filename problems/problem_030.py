# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) < 2:
        return None
    values.sort()
    return values[1]


# Unit test
def find_second_largest_test():
    assert find_second_largest([]) is None
    assert find_second_largest([90, 95, 92, 89]) == 90
    assert find_second_largest([88, 87, 91, 81]) == 87
    assert find_second_largest([20, 47, 00, 11]) == 11
    print("End of unit tests")


find_second_largest_test()
