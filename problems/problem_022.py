# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    item_list = []
    if not is_sunny and is_workday:
        item_list.append('umbrella')
    if is_workday:
        item_list.append('laptop')
    if not is_workday:
        item_list.append('surfboard')
    return item_list


# Unit test
def gear_for_day_test():
    assert "surfboard" in gear_for_day(0, 0)
    assert "umbrella" not in gear_for_day(0, 1)
    assert "umbrella" in gear_for_day(1, 0)
    assert "laptop" in gear_for_day(1, 1)
    print("End of unit tests")


gear_for_day_test()
