# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem
def shift_letters(word):
    letters = "abcdefghijklmnopqrstuvwxyz"
    out = ''
    for letter in word:
        tmp = letters[(letters.index(letter.lower()) + 1) % len(letters)]
        if letter.isupper():
            tmp = tmp.upper()
        out += tmp
    return out


# Unit test
def shift_letters_test():
    assert shift_letters("import") == "jnqpsu"
    assert shift_letters("ABBA") == "BCCB"
    assert shift_letters("Kala") == "Lbmb"
    assert shift_letters("zap") == "abq"
    print("End of unit tests")


shift_letters_test()
