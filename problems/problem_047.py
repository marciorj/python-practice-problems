# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    pass_len = len(password)
    if pass_len > 12 or pass_len < 6:
        return False
    special_chars = ['$', '!', '@']
    ok_lower = False
    ok_upper = False
    ok_digit = False
    ok_special = False
    for el in password:
        if el.isupper():
            ok_upper = True
        elif el.islower():
            ok_lower = True
        elif el.isdigit():
            ok_digit = True
        elif el in special_chars:
            ok_special = True
        if ok_special and ok_digit and ok_lower and ok_upper:
            return True
    return ok_special and ok_digit and ok_lower and ok_upper


# Unit tests
def check_password_test():
    assert check_password("aA1@__") is True
    assert check_password("aA1@kwefhgkuwef") is False
    print("End of unit tests")


check_password_test()
