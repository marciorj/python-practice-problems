# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

class BankAccount:
    def __init__(self, balance):
        self.balance = balance

    def get_balance(self):
        return self.balance

    def deposit(self, val):
        self.balance += val
        return self

    def withdraw(self, val):
        self.balance -= val
        return self


def class_bankAccount_test():
    account = BankAccount(100)
    assert account.get_balance() == 100
    account.withdraw(50)
    assert account.get_balance() == 50
    account.deposit(120)
    assert account.get_balance() == 170
    print("End of unit tests")


class_bankAccount_test()
