# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7

def sum_two_numbers(n1, n2):
    return n1 + n2


# Unit tests
def sum_two_numbers_test():
    assert sum_two_numbers(3, 4) == 7
    print("End of unit tests")


sum_two_numbers_test()
