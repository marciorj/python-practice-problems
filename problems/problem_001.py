# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def minimum_value(value1, value2):
    return min(value1, value2)


# Unit test
def minimum_value_test():
    assert minimum_value(2, 7) == 2
    assert minimum_value(101, 99) == 99
    assert minimum_value(88, 88) == 88
    print("End of unit tests")


minimum_value_test()
