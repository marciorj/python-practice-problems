# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.


def add_csv_lines(csv_lines):
    def sum_up(val):
        return sum(map(lambda el: int(el), val.split(',')))
    return list(map(sum_up, csv_lines))


# Unit tests
def add_csv_lines_test():
    assert add_csv_lines([]) == []
    assert add_csv_lines(["3", "1,9"]) == [3, 10]
    assert add_csv_lines(["8,1,7", "10,10,10", "1,2,3"]) == [16, 30, 6]
    print("End of unit tests")


add_csv_lines_test()
