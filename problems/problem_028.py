# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    out = []
    for xx in s:
        if xx in out:
            continue
        out.append(xx)
    return ''.join(out)


# Unit test
def remove_duplicate_letters_test():
    assert remove_duplicate_letters('abc') == "abc"
    assert remove_duplicate_letters('abcabc') == "abc"
    assert remove_duplicate_letters('abccba') == "abc"
    assert remove_duplicate_letters('abccbad') == "abcd"
    print("End of unit tests")


remove_duplicate_letters_test()
