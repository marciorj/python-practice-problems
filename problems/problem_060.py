# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]

def only_odds(num_list):
    return list(filter(lambda x: x % 2, num_list))


# Unit test
def only_odds_test():
    assert only_odds([1, 2, 3, 4]) == [1, 3]
    assert only_odds([2, 4, 6, 8]) == []
    assert only_odds([1, 3, 5, 7]) == [1, 3, 5, 7]
    print("End of unit tests")


only_odds_test()
