# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(val):
    out = 0
    for ii in range(val):
        out += (1 + ii)/(2 + ii)
    return out


# Unit test
def sum_fraction_sequence_test():
    assert sum_fraction_sequence(1) == 1/2
    assert sum_fraction_sequence(2) == 1/2 + 2/3
    assert sum_fraction_sequence(3) == 1/2 + 2/3 + 3/4
    print("End of unit tests")


sum_fraction_sequence_test()
