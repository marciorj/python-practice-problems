# Write a function that meets these requirements.
#
# Name:       num_concat
# Parameters: two numerical parameters
# Returns:    the concatenated string conversions
#             of the numerical parameters
#
# Examples:
#     input:
#       parameter 1: 3
#       parameter 2: 10
#     returns: "310"
#     input:
#       parameter 1: 9238
#       parameter 2: 0
#     returns: "92380"

def num_concat(n1, n2):
    return str(n1) + str(n2)


# Unit test
def num_concat_test():
    assert num_concat(3, 10) == "310"
    assert num_concat(9238, 0) == "92380"
    print("End of unit tests")


num_concat_test()
