# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2
from functools import reduce


def count_letters_and_digits(s):
    return reduce(
        lambda ac, el: (
            ac[0], ac[1] + 1) if el.isdigit() else (ac[0] + 1, ac[1]),
        list(s),
        (0, 0))


# Unit tests
def count_letters_and_digits_test():
    assert count_letters_and_digits("") == (0, 0)
    assert count_letters_and_digits("a") == (1, 0)
    assert count_letters_and_digits("1") == (0, 1)
    assert count_letters_and_digits("1a") == (1, 1)
    print("End of unit tests")


count_letters_and_digits_test()
