# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    ori = list(word.lower())
    rev = list(word.lower())
    rev.reverse()
    return ori == rev


# Unit test
def is_palindrome_test():
    assert is_palindrome("XXX") is True
    assert is_palindrome("test") is False
    assert is_palindrome("Carac") is True
    assert is_palindrome("Carrac") is True
    print("End of unit tests")


is_palindrome_test()
