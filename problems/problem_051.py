# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!
import math


def safe_divide(num, den):
    if den == 0:
        return math.inf
    return num / den


# Unit test
def safe_divide_test():
    assert safe_divide(5, 0) == math.inf
    assert safe_divide(0, 5) == 0
    assert safe_divide(4, 2) == 2.0
    print("End of unit tests")


safe_divide_test()
