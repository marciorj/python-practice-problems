# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the n is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
from functools import reduce


def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    return reduce(lambda ac, el: el + ac, range(0, 2 * n + 1, 2), 0)


# Unit test
def sum_of_first_n_even_numbers_test():
    assert sum_of_first_n_even_numbers(-1) is None
    assert sum_of_first_n_even_numbers(0) == 0
    assert sum_of_first_n_even_numbers(1) == 2
    assert sum_of_first_n_even_numbers(2) == 6
    assert sum_of_first_n_even_numbers(5) == 30
    print("End of unit tests")


sum_of_first_n_even_numbers_test()
