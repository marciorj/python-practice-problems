# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_5(number):
    return 'fizz' if number and not number % 5 else number


# Unit test
def is_divisible_by_5_test():
    assert is_divisible_by_5(0) == 0
    assert is_divisible_by_5(1) == 1
    assert is_divisible_by_5(5) == 'fizz'
    assert is_divisible_by_5(245) == 'fizz'
    assert is_divisible_by_5(7) == 7
    print("End of unit tests")


is_divisible_by_5_test()
