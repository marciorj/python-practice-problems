# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    x_max = rect_x + rect_width
    y_max = rect_y + rect_height
    if x < rect_x or x > x_max:
        return False
    if y < rect_y or y > y_max:
        return False
    return True


# Unit test
def is_inside_bounds_test():
    assert is_inside_bounds(-1, 15, 2, 3, 6, 4) is False
    assert is_inside_bounds(-1, 4, 2, 3, 6, 4) is False
    assert is_inside_bounds(4, 15, 2, 3, 6, 4) is False
    assert is_inside_bounds(4, 4, 2, 3, 6, 4) is True
    print("End of unit tests")


is_inside_bounds_test()
