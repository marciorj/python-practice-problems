# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
    div_by_3 = number % 3 == 0
    div_by_5 = number % 5 == 0
    if div_by_3 and div_by_5:
        return "fizzbuzz"
    elif div_by_3:
        return "fizz"
    elif div_by_5:
        return "buzz"
    return number


# Unit test
def fizzbuzz_test():
    assert fizzbuzz(3) == 'fizz'
    assert fizzbuzz(5) == 'buzz'
    assert fizzbuzz(15) == 'fizzbuzz'
    assert fizzbuzz(17) == 17
    print("End of unit tests")


fizzbuzz_test()
