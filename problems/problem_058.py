# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

from functools import reduce


def group_cities_by_state(city_list):
    def processCity(ac, el):
        city, state = map(lambda x: x.strip(), el.split(","))
        tmp = ac.get(state, [])
        tmp.append(city)
        ac[state] = tmp
        return ac
    return reduce(processCity, city_list, {})


# Unit test
def group_cities_by_state_test():
    assert group_cities_by_state(
        ["San Antonio, TX"]) == {"TX": ["San Antonio"]}
    assert group_cities_by_state(
        ["Springfield, MA", "Boston, MA"]) == {"MA": ["Springfield", "Boston"]}
    assert group_cities_by_state(
        ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]) == \
        {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
    print("End of unit tests")


group_cities_by_state_test()
