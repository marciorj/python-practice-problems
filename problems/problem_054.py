# Write a function that meets these requirements.
#
# Name:       check_input
# Parameters: one parameter that can hold any value
# Returns:    if the value of the parameter is the
#             string "raise", then it should raise
#             a ValueError. otherwise, it should
#             just return the value of the parameter
#
# Examples
#    * input:   3
#      returns: 3
#    * input:   "this is a string"
#      returns: "this is a string"
#    * input:   "raise"
#      RAISES:  ValueError

def check_input(value):
    if value != "raise":
        return value
    raise ValueError


# Unit test
def check_input_test():
    assert check_input(3) == 3
    assert check_input("this is a string") == "this is a string"
    try:
        assert check_input("raise")
    except ValueError:
        return
    print("Failed to raise ValueError exception on 'raise'")


check_input_test()
print("End of unit tests")
