# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one

import random


def specific_random():
    numbers = [xx for xx in range(10, 501) if xx % 5 == 0 and xx % 7 == 0]
    return numbers[random.randrange(len(numbers))]


# Unit test
def specific_random_test():
    for ii in range(5):
        print("specific_random returned: ", specific_random())
    print("End of unit tests")


specific_random_test()
