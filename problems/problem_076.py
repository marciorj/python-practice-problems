# Modify the withdraw method of BankAccount so that the bank
# account can not have a negative balance.
#
# If a person tries to withdraw more than what is in the
# balance, then the method should raise a ValueError.

class BankAccount:
    def __init__(self, balance):
        self.balance = balance

    def get_balance(self):
        return self.balance

    def withdraw(self, amount):
        # If the amount is more than what is in
        # the balance, then raise a ValueError
        if (amount > self.balance):
            raise ValueError("Not enough funds")
        self.balance -= amount

    def deposit(self, amount):
        self.balance += amount


def class_bankAccount_test():
    account = BankAccount(100)
    assert account.get_balance() == 100
    account.withdraw(50)
    assert account.get_balance() == 50
    account.deposit(120)
    assert account.get_balance() == 170
    try:
        account.withdraw(account.get_balance() + 100)
    except ValueError:
        print("End of unit tests")
        return
    assert False, "Failed to prevent withdrawing more than available"


class_bankAccount_test()
