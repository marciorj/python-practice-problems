# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    return max(value1, value2, value3)


# Unit test
def max_of_three_test():
    assert max_of_three(2, 7, 19) == 19
    assert max_of_three(22, 1, 5) == 22
    assert max_of_three(43, 798, 3) == 798
    print("End of unit tests")


max_of_three_test()
