# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if len(values) == 0:
        return None
    average = sum(values) / len(values)
    if average >= 90:
        return "A"
    if average >= 80:
        return "B"
    if average >= 70:
        return "C"
    if average >= 60:
        return "D"
    return "F"


# Unit test
def calculate_grade_test():
    assert calculate_grade([]) is None
    assert calculate_grade([90, 95, 92, 89]) == "A"
    assert calculate_grade([88, 87, 91, 81]) == "B"
    assert calculate_grade([79, 80, 70, 70]) == "C"
    assert calculate_grade([71, 59, 60, 65]) == "D"
    assert calculate_grade([20, 47, 00, 11]) == "F"
    print("End of unit tests")


calculate_grade_test()
